# Example repo for demonstaring how a deep folder structure looks like in file browser (in MR)

The example represents a common package structure in a Java project. In the Java world, it's completely common to have this or even deeper structure of packages. 

See file browser in https://gitlab.com/zkovari/demonstration-for-deep-folder-structure-in-file-browser/merge_requests/1

